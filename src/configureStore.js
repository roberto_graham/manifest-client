import { applyMiddleware, createStore } from 'redux';
import createRootReducer from './reducers';
import { routerMiddleware } from 'connected-react-router';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';

export default function configureStore(history) {
    return createStore(
        createRootReducer(history),
        composeWithDevTools(
            applyMiddleware(
                routerMiddleware(history)
            )
        )
    );
};