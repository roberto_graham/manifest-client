export default {
    CONTAINER_TOPIC_UPDATE: '/topic/container',
    CONTAINER_QUEUE_UPDATE: '/user/queue/container',
    CONTAINER_QUEUE_UPDATE_ERROR: '/user/queue/container/error',
    CONTAINER_KILL: '/app/container/kill',
    CONTAINER_STOP: '/app/container/stop',
    CONTAINER_PAUSE: '/app/container/pause',
    CONTAINER_UNPAUSE: '/app/container/unpause',
    CONTAINER_START: '/app/container/start',
    CONTAINER_RESTART: '/app/container/restart',
    UPDATE_NAME_AND_ID_FILTER: 'UPDATE_NAME_AND_ID_FILTER',
    TURN_STATE_ON: 'TURN_STATE_ON',
    TURN_STATE_OFF: 'TURN_STATE_OFF',
    PIN_CONTAINER: 'PIN_CONTAINER',
    UNPIN_CONTAINER: 'UNPIN_CONTAINER',
    ACTIONS: {
        KILL: 'Kill',
        STOP: 'Stop',
        PAUSE: 'Pause',
        UNPAUSE: 'Unpause',
        START: 'Start',
        RESTART: 'Restart'
    },
    STATES: {
        RUNNING: 'running',
        PAUSED: 'paused',
        EXITED: 'exited',
        DEAD: 'dead',
        CREATED: 'created',
        RESTARTING: 'restarting'
    }
};