import socket from './socket';
import container from './container';
import image from './image';

export default {
    socket,
    container,
    image
};
