import constants from '../constants';

const updateNameAndIdFilter = (nameAndIdFilter) => {
    return {
        type: constants.container.UPDATE_NAME_AND_ID_FILTER,
        data: nameAndIdFilter
    };
};

const turnStateOn = (state) => {
    return {
        type: constants.container.TURN_STATE_ON,
        data: state
    };
};

const turnStateOff = (state) => {
    return {
        type: constants.container.TURN_STATE_OFF,
        data: state
    };
};

const pinContainer = (containerId) => {
    return {
        type: constants.container.PIN_CONTAINER,
        data: containerId
    };
};

const unpinContainer = (containerId) => {
    return {
        type: constants.container.UNPIN_CONTAINER,
        data: containerId
    };
};

export default {
    updateNameAndIdFilter,
    turnStateOn,
    turnStateOff,
    pinContainer,
    unpinContainer
};
