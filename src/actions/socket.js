import constants from '../constants';

const subscribeContainers = () => {
    return {
        type: constants.socket.SUBSCRIBE_CONTAINERS
    };
};

const subscribeContainerStatistics = (containerId) => {
    return {
        type: constants.socket.SUBSCRIBE_CONTAINER_STATISTICS,
        data: containerId
    };
}

const subscribeImageInfo = (imageId) => {
    return {
        type: constants.socket.SUBSCRIBE_IMAGE_INFO,
        data: imageId
    };
}

const unsubscribeAll = () => {
    return {
        type: constants.socket.UNSUBSCRIBE_ALL
    };
};

const receiveMessage = (topic, data) => {
    return {
        type: topic,
        data
    };
};

const updateConnectedStatus = (connected) => {
    return {
        type: constants.socket.UPDATE_CONNECTED_STATUS,
        data: connected
    };
};

const pushToMessageQueue = (topic, data) => {
    return {
        type: constants.socket.PUSH_TO_MESSAGE_QUEUE,
        data: {
            topic,
            data
        }
    };
};

const shiftMessageQueue = () => {
    return {
        type: constants.socket.SHIFT_MESSAGE_QUEUE
    };
};


export default {
    subscribeContainers,
    subscribeContainerStatistics,
    subscribeImageInfo,
    receiveMessage,
    updateConnectedStatus,
    unsubscribeAll,
    pushToMessageQueue,
    shiftMessageQueue
};
