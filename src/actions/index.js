import socket from './socket';
import container from './container';

export default {
    socket,
    container
};
