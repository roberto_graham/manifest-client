import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Client } from "@stomp/stompjs";

class StompClient extends Component {

    constructor(props) {
        super(props);

        this.onConnect = this.onConnect.bind(this);
        this.onDisconnect = this.onDisconnect.bind(this);
        this.onWebSocketClose = this.onWebSocketClose.bind(this);
        this.subscribeTopic = this.subscribeTopic.bind(this);
        this.unsubscribeTopic = this.unsubscribeTopic.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.deleteTopic = this.deleteTopic.bind(this);

        const { location } = window;
        const { hostname, port } = location;
        const url = `ws://${hostname}:${port}/ws`;

        this.client = new Client({
            brokerURL: url,
            onConnect: this.onConnect,
            onDisconnect: this.onDisconnect,
            onWebSocketClose: this.onWebSocketClose
        });

        this.topicToSubscriptionMap = {};

        this.client.activate();
    }

    componentDidUpdate(prevProps) {
        const { topics, connected, messageQueue } = this.props;

        if (connected) {
            Object.keys(this.topicToSubscriptionMap)
                .filter(topic => topics.indexOf(topic) === -1)
                .forEach(topic => {
                    this.unsubscribeTopic(topic);
                    this.deleteTopic(topic);
                });

            topics.filter(topic => !this.topicToSubscriptionMap.hasOwnProperty(topic))
                .forEach(topic => this.subscribeTopic(topic));

            if (messageQueue[0])
                this.sendMessage(messageQueue[0]);
        }
    }

    onConnect(frame) {
        const { actions } = this.props;
        const { updateConnectedStatus } = actions;

        updateConnectedStatus(true);
    }

    onDisconnect(frame) {
        const { actions } = this.props;
        const { updateConnectedStatus } = actions;

        updateConnectedStatus(false);

        Object.keys(this.topicToSubscriptionMap)
            .forEach(topic => this.deleteTopic(topic));
    }

    onWebSocketClose(closeEvent) {
        const { actions } = this.props;
        const { updateConnectedStatus } = actions;

        updateConnectedStatus(false);

        Object.keys(this.topicToSubscriptionMap)
            .forEach(topic => this.deleteTopic(topic));
    }

    subscribeTopic(topic) {
        const { actions } = this.props;
        const { receiveMessage } = actions;

        this.topicToSubscriptionMap[topic] = this.client.subscribe(
            topic,
            message => {
                let messageBody = message.body;

                try {
                    messageBody = JSON.parse(messageBody);
                } catch (ignored) {
                }

                receiveMessage(topic, messageBody);
            }
        );
    }

    unsubscribeTopic(topic) {
        this.topicToSubscriptionMap[topic].unsubscribe();
    }

    deleteTopic(topic) {
        delete this.topicToSubscriptionMap[topic];
    }

    sendMessage(message) {
        const { actions } = this.props;
        const { shiftMessageQueue } = actions;
        
        try {
            this.client.publish({ destination: message.topic, body: message.data });
            shiftMessageQueue();
        } catch (ignored) {
        }
    }

    render() {
        return <React.Fragment />;
    }
}

StompClient.propTypes = {
    topics: PropTypes.arrayOf(PropTypes.string).isRequired,
    actions: PropTypes.shape({
        receiveMessage: PropTypes.func.isRequired,
        updateConnectedStatus: PropTypes.func.isRequired,
        shiftMessageQueue: PropTypes.func.isRequired
    }).isRequired,
    messageQueue: PropTypes.arrayOf(PropTypes.exact({
        topic: PropTypes.string,
        data: PropTypes.any
    })).isRequired,
    connected: PropTypes.bool.isRequired
};

export default StompClient;