import React, { Component } from 'react';
import { Typography } from '@rmwc/typography';
import '@material/typography/dist/mdc.typography.css';
import { List, ListDivider, SimpleListItem } from '@rmwc/list';
import '@material/list/dist/mdc.list.css';
import { Card } from '@rmwc/card';
import '@material/card/dist/mdc.card.css';
import '@material/button/dist/mdc.button.css';
import '@material/icon-button/dist/mdc.icon-button.css';
import PropTypes from 'prop-types';

class ContainerImageCard extends Component {

    render() {
        const { Image, ImageID } = this.props;

        return (
            <Card>
                <div style={{ padding: '0 1rem 0 1rem' }}>
                    <Typography
                        use="subtitle1"
                        tag="h2"
                        style={{
                            textOverflow: 'ellipsis',
                            whiteSpace: 'nowrap',
                            overflow: 'hidden'
                        }}>
                        Image
                    </Typography>
                </div>
                <ListDivider />
                <List
                    twoLine
                    dense
                    nonInteractive>
                    <SimpleListItem
                        ripple={false}
                        text={Image}
                        secondaryText={ImageID} />
                </List>
            </Card >
        );
    }
}

ContainerImageCard.propTypes = {
    Image: PropTypes.string.isRequired,
    ImageID: PropTypes.string.isRequired
};

export default ContainerImageCard;