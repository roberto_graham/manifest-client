import React, { Component } from 'react';
import { Typography } from '@rmwc/typography';
import '@material/typography/dist/mdc.typography.css';
import { Grid, GridCell } from '@rmwc/grid';
import '@material/layout-grid/dist/mdc.layout-grid.css';
import { Link } from 'react-router-dom';
import Icon from '@mdi/react';
import { mdiPowerPlug, mdiPowerPlugOff } from '@mdi/js';
import PropTypes from 'prop-types';
import '../../css/AppTitle.css';

class AppTitle extends Component {

    render() {
        const { connected, pathname } = this.props;
        const title = `manifest${pathname}`;

        return (
            <Grid
                style={{ paddingBottom: '0' }}>
                <GridCell span={12}>
                    <div className="AppTitle-header">
                        <div className="AppTitle-connected-icon-wrapper">
                            <Icon
                                path={connected ? mdiPowerPlug : mdiPowerPlugOff}
                                size={1}
                                color="rgba(0, 0, 0, 0.87)"
                                style={{ paddingRight: '.5rem' }} />
                        </div>
                        <Typography
                            use="headline5"
                            tag={Link}
                            to="/"
                            theme="textPrimaryOnBackground"
                            style={{
                                textDecoration: 'none',
                                textOverflow: 'ellipsis',
                                whiteSpace: 'nowrap',
                                overflow: 'hidden'
                            }}>
                            {title}
                        </Typography>
                    </div>
                </GridCell>
            </Grid>
        );
    }
}

AppTitle.propTypes = {
    connected: PropTypes.bool.isRequired,
    pathname: PropTypes.string.isRequired
};

export default AppTitle;