import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@rmwc/textfield';
import '@material/textfield/dist/mdc.textfield.css';
import '@material/floating-label/dist/mdc.floating-label.css';
import '@material/notched-outline/dist/mdc.notched-outline.css';
import '@material/line-ripple/dist/mdc.line-ripple.css';
import { GridCell } from '@rmwc/grid';
import '@material/layout-grid/dist/mdc.layout-grid.css';
import { Chip, ChipSet } from '@rmwc/chip';
import '@material/chips/dist/mdc.chips.css';

class ContainerFilterer extends Component {

    constructor(props) {
        super(props);
        this.onNameAndIdFilterTextFieldChanged = this.onNameAndIdFilterTextFieldChanged.bind(this);
        this.onStateCheckBoxChanged = this.onStateCheckBoxChanged.bind(this);
    }

    onNameAndIdFilterTextFieldChanged(event) {
        const { actions } = this.props;
        const { updateNameAndIdFilter } = actions;
        updateNameAndIdFilter(event.target.value);
    }

    onStateCheckBoxChanged(event) {
        const { actions, filteredOutStates } = this.props;
        const { turnStateOn, turnStateOff } = actions;
        const { detail } = event;
        const { chipId } = detail;
        if (filteredOutStates.includes(chipId))
            turnStateOn(chipId);
        else
            turnStateOff(chipId);
    }

    render() {
        const { nameAndIdFilter, states, filteredOutStates, stateCounts } = this.props;
        return (
            <React.Fragment>
                <GridCell
                    span={1}
                    phone={4}
                    tablet={4}
                    desktop={6}
                    style={{
                        display: 'flex',
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start'
                    }}>
                    <TextField
                        placeholder="Name/ID"
                        fullwidth
                        value={nameAndIdFilter}
                        onChange={this.onNameAndIdFilterTextFieldChanged} />
                </GridCell>
                <GridCell
                    span={1}
                    phone={4}
                    tablet={4}
                    desktop={6}
                    style={{
                        display: 'flex',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        flexWrap: 'wrap'
                    }}>
                    <ChipSet filter>
                        {states.sort()
                            .map(state =>
                                <Chip
                                    key={state}
                                    selected={!filteredOutStates.includes(state)}
                                    onInteraction={this.onStateCheckBoxChanged}
                                    checkmark
                                    label={`${state} (${stateCounts[state]})`} />
                            )}
                    </ChipSet>
                </GridCell>
            </React.Fragment>
        );
    }
}

ContainerFilterer.propTypes = {
    nameAndIdFilter: PropTypes.string.isRequired,
    states: PropTypes.arrayOf(PropTypes.string).isRequired,
    filteredOutStates: PropTypes.arrayOf(PropTypes.string).isRequired,
    actions: PropTypes.shape({
        updateNameAndIdFilter: PropTypes.func.isRequired,
        turnStateOn: PropTypes.func.isRequired,
        turnStateOff: PropTypes.func.isRequired
    }).isRequired,
    stateCounts: PropTypes.objectOf(PropTypes.number).isRequired
};

export default ContainerFilterer;