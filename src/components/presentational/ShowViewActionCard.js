import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Card, CardActionButton, CardActionButtons, CardActions } from '@rmwc/card';
import '@material/card/dist/mdc.card.css';
import '@material/button/dist/mdc.button.css';
import '@material/icon-button/dist/mdc.icon-button.css';
import { Typography } from '@rmwc/typography';
import '@material/typography/dist/mdc.typography.css';

class ShowViewActionCard extends Component {

    render() {
        const { title, link, count } = this.props;

        return (
            <Card>
                <div style={{ padding: '0 1rem 0 1rem' }}>
                    <Typography
                        use="headline6"
                        tag="h2"
                        theme="textPrimaryOnBackground"
                        style={{
                            textOverflow: 'ellipsis',
                            overflow: 'hidden',
                            marginBottom: '0.5rem'
                        }}>
                        {title}
                    </Typography>
                    <Typography
                        use="headline3"
                        tag="h3"
                        theme="textPrimaryOnBackground"
                        style={{
                            textOverflow: 'ellipsis',
                            overflow: 'hidden',
                            margin: '0'
                        }}>
                        {count}
                    </Typography>
                </div>
                <CardActions>
                    <CardActionButtons>
                        <Link
                            to={link}
                            style={{ textDecoration: 'none' }}>
                            <CardActionButton>
                                List all
                            </CardActionButton>
                        </Link>
                    </CardActionButtons>
                </CardActions>
            </Card>
        );
    }
}

ShowViewActionCard.propTypes = {
    disabled: PropTypes.bool,
    title: PropTypes.string,
    link: PropTypes.string,
    count: PropTypes.number
};

export default ShowViewActionCard;