import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import { Card, CardActions, CardPrimaryAction, CardActionIcons, CardActionIcon } from '@rmwc/card';
import '@material/card/dist/mdc.card.css';
import '@material/button/dist/mdc.button.css';
import '@material/icon-button/dist/mdc.icon-button.css';
import { List, ListDivider, SimpleListItem } from '@rmwc/list';
import '@material/list/dist/mdc.list.css';
import constants from '../../constants';
import { Typography } from '@rmwc/typography';
import '@material/typography/dist/mdc.typography.css';
import { SimpleMenu } from '@rmwc/menu';
import '@material/menu/dist/mdc.menu.css';
import '@material/menu-surface/dist/mdc.menu-surface.css';
import '@material/list/dist/mdc.list.css';
import TimeAgo from "react-timeago";
import Highlighter from "react-highlight-words";
import { mdiPin, mdiPinOutline, mdiDotsVertical } from '@mdi/js';

const { STATES, ACTIONS } = constants.container;

const STATE_ACTIONS_LOOKUP = {
    [STATES.PAUSED]: [ACTIONS.UNPAUSE, ACTIONS.STOP, ACTIONS.KILL, ACTIONS.RESTART],
    [STATES.RUNNING]: [ACTIONS.PAUSE, ACTIONS.STOP, ACTIONS.KILL, ACTIONS.RESTART],
    [STATES.EXITED]: [ACTIONS.START, ACTIONS.RESTART],
    [STATES.DEAD]: [ACTIONS.START, ACTIONS.RESTART],
    [STATES.RESTARTING]: [ACTIONS.PAUSE, ACTIONS.STOP, ACTIONS.KILL, ACTIONS.RESTART]
};

class ContainerCard extends Component {

    constructor(props) {
        super(props);
        this.highlightedComponent = this.highlightedComponent.bind(this);
        this.onPinIconButtonToggled = this.onPinIconButtonToggled.bind(this);
    }

    highlightedComponent(text) {
        const { filterText } = this.props;
        return <Highlighter
            autoEscape={true}
            caseSensitive={false}
            searchWords={[filterText]}
            textToHighlight={text} />
    }

    onPinIconButtonToggled(event) {
        const { container, actions } = this.props;
        const { Id } = container;
        const { pinContainer, unpinContainer } = actions;
        if (event.target.isOn)
            pinContainer(Id);
        else
            unpinContainer(Id);
    }

    render() {
        const { container, disableActions, pinnedContainerIds, actionFunctions } = this.props;
        const { Id, Image, Command, Created, Status, Names, State } = container;
        const createdMillis = Created * 1e3;
        const link = `/containers/${Id}/`;
        const availableActions = STATE_ACTIONS_LOOKUP[State];

        return (
            <Card>
                <CardPrimaryAction
                    tag={Link}
                    to={link}>
                    <div style={{ padding: '0 1rem 0 1rem' }}>
                        <Typography
                            use="subtitle1"
                            tag="h2"
                            style={{
                                textOverflow: 'ellipsis',
                                whiteSpace: 'nowrap',
                                overflow: 'hidden'
                            }}>
                            {this.highlightedComponent(Names.join(', '))}
                        </Typography>
                        <Typography
                            use="subtitle2"
                            tag="h3"
                            theme="textSecondaryOnBackground"
                            style={{
                                marginTop: '-1rem',
                                textOverflow: 'ellipsis',
                                whiteSpace: 'nowrap',
                                overflow: 'hidden'
                            }}>
                            {this.highlightedComponent(Image)}
                        </Typography>
                    </div>
                </CardPrimaryAction>
                <ListDivider />
                <List
                    twoLine
                    dense
                    nonInteractive>
                    <SimpleListItem
                        ripple={false}
                        text={"id"}
                        secondaryText={this.highlightedComponent(Id)} />
                    <SimpleListItem
                        ripple={false}
                        text={"command"}
                        secondaryText={Command} />
                    <SimpleListItem
                        ripple={false}
                        text={"created"}
                        secondaryText={<TimeAgo date={createdMillis} />} />
                    <SimpleListItem
                        ripple={false}
                        text={"status"}
                        secondaryText={Status} />
                </List>
                <CardActions>
                    <CardActionIcons>
                        <CardActionIcon
                            checked={pinnedContainerIds.includes(Id)}
                            onChange={this.onPinIconButtonToggled}
                            onIcon={{
                                strategy: 'component',
                                icon:
                                    <svg>
                                        <path d={mdiPin} />
                                    </svg>
                            }}
                            icon={{
                                strategy: 'component',
                                icon:
                                    <svg>
                                        <path d={mdiPinOutline} />
                                    </svg>
                            }} />
                        {availableActions && availableActions.length ?
                            <SimpleMenu handle={
                                <CardActionIcon icon={{
                                    strategy: 'component',
                                    icon:
                                        <svg>
                                            <path d={mdiDotsVertical} />
                                        </svg>
                                }} />
                            } >
                                {availableActions.map(action =>
                                    <SimpleListItem
                                        disabled={disableActions}
                                        key={action}
                                        onClick={(event) => actionFunctions[action](Id)}
                                        text={action} />
                                )}
                            </SimpleMenu>
                            : <React.Fragment />}
                    </CardActionIcons>
                </CardActions>
            </Card >
        );
    }
}

ContainerCard.propTypes = {
    container: PropTypes.shape({
        Id: PropTypes.string,
        Names: PropTypes.arrayOf(PropTypes.string),
        Image: PropTypes.string,
        ImageID: PropTypes.string,
        Command: PropTypes.string,
        Created: PropTypes.number,
        State: PropTypes.string,
        Status: PropTypes.string,
        Ports: PropTypes.arrayOf(PropTypes.shape({
            PrivatePort: PropTypes.number,
            PublicPort: PropTypes.number,
            Type: PropTypes.string,
            IP: PropTypes.string
        })),
        Labels: PropTypes.objectOf(PropTypes.string),
        SizeRw: PropTypes.number,
        SizeRootFs: PropTypes.number,
        NetworkSettings: PropTypes.shape({
            IPAddress: PropTypes.string,
            IPPrefixLen: PropTypes.number,
            Gateway: PropTypes.string,
            Bridge: PropTypes.string,
            PortMapping: PropTypes.objectOf(PropTypes.objectOf(PropTypes.string)),
            Ports: PropTypes.objectOf(PropTypes.shape({
                HostIp: PropTypes.string,
                HostPort: PropTypes.string
            })),
            MacAddress: PropTypes.string,
            Networks: PropTypes.objectOf(PropTypes.shape({
                Aliases: PropTypes.arrayOf(PropTypes.string),
                NetworkID: PropTypes.string,
                EndpointID: PropTypes.string,
                Gateway: PropTypes.string,
                IPAddress: PropTypes.string,
                IPPrefixLen: PropTypes.number,
                IPv6Gateway: PropTypes.string,
                GlobalIPv6Address: PropTypes.string,
                GlobalIPv6PrefixLen: PropTypes.number,
                MacAddress: PropTypes.string
            })),
            EndpointID: PropTypes.string,
            SandboxID: PropTypes.string,
            SandboxKey: PropTypes.string,
            HairpinMode: PropTypes.bool,
            LinkLocalIPv6Address: PropTypes.string,
            LinkLocalIPv6PrefixLen: PropTypes.number,
            GlobalIPv6Address: PropTypes.string,
            GlobalIPv6PrefixLen: PropTypes.number,
            IPv6Gateway: PropTypes.string
        }),
        Mounts: PropTypes.arrayOf(PropTypes.shape({
            Type: PropTypes.string,
            Name: PropTypes.string,
            Source: PropTypes.string,
            Destination: PropTypes.string,
            Driver: PropTypes.string,
            Mode: PropTypes.string,
            RW: PropTypes.bool,
            Propagation: PropTypes.string
        }))
    }).isRequired,
    actionFunctions: PropTypes.objectOf(PropTypes.func.isRequired),
    disableActions: PropTypes.bool.isRequired,
    filterText: PropTypes.string.isRequired,
    actions: PropTypes.shape({
        pinContainer: PropTypes.func.isRequired,
        unpinContainer: PropTypes.func.isRequired
    }).isRequired,
    pinnedContainerIds: PropTypes.arrayOf(PropTypes.string).isRequired
};

export default ContainerCard;