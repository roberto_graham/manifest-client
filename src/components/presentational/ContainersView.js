import React, { Component } from 'react';
import PropTypes from 'prop-types';
import constants from '../../constants';
import { Grid, GridCell } from '@rmwc/grid';
import '@material/layout-grid/dist/mdc.layout-grid.css';
import { Dialog, DialogActions, DialogButton, DialogContent, DialogTitle } from '@rmwc/dialog';
import '@material/dialog/dist/mdc.dialog.css';
import '@material/button/dist/mdc.button.css';
import { TextField } from "@rmwc/textfield";
import '@material/textfield/dist/mdc.textfield.css';
import '@material/floating-label/dist/mdc.floating-label.css';
import '@material/notched-outline/dist/mdc.notched-outline.css';
import '@material/line-ripple/dist/mdc.line-ripple.css';
import { Select } from '@rmwc/select';
import '@material/select/dist/mdc.select.css';
import ContainerActionDialog from "./ContainerActionDialog";
import ContainerFiltererContainer from '../containers/ContainerFiltererContainer';
import ContainerCardContainer from '../containers/ContainerCardContainer';

class ContainersView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            killContainerDialogOpen: false,
            stopContainerDialogOpen: false,
            pauseContainerDialogOpen: false,
            unpauseContainerDialogOpen: false,
            startContainerDialogOpen: false,
            restartContainerDialogOpen: false,
            dialogContainerId: '',
            killSignal: '',
            stopWaitSeconds: '10',
            restartWaitSeconds: '10',
            errorDialogOpen: false
        };

        this.openKillContainerDialog = this.openKillContainerDialog.bind(this);
        this.openStopContainerDialog = this.openStopContainerDialog.bind(this);
        this.openPauseContainerDialog = this.openPauseContainerDialog.bind(this);
        this.openUnpauseContainerDialog = this.openUnpauseContainerDialog.bind(this);
        this.openStartContainerDialog = this.openStartContainerDialog.bind(this);
        this.openRestartContainerDialog = this.openRestartContainerDialog.bind(this);
        this.closeKillContainerDialog = this.closeKillContainerDialog.bind(this);
        this.closeStopContainerDialog = this.closeStopContainerDialog.bind(this);
        this.closePauseContainerDialog = this.closePauseContainerDialog.bind(this);
        this.closeUnpauseContainerDialog = this.closeUnpauseContainerDialog.bind(this);
        this.closeStartContainerDialog = this.closeStartContainerDialog.bind(this);
        this.closeRestartContainerDialog = this.closeRestartContainerDialog.bind(this);
        this.closeErrorDialog = this.closeErrorDialog.bind(this);
        this.killContainer = this.killContainer.bind(this);
        this.stopContainer = this.stopContainer.bind(this);
        this.pauseContainer = this.pauseContainer.bind(this);
        this.unpauseContainer = this.unpauseContainer.bind(this);
        this.startContainer = this.startContainer.bind(this);
        this.restartContainer = this.restartContainer.bind(this);
        this.onKillSignalMenuOptionChange = this.onKillSignalMenuOptionChange.bind(this);
        this.onStopWaitSecondsTextFieldChange = this.onStopWaitSecondsTextFieldChange.bind(this);
        this.onRestartWaitSecondsTextFieldChange = this.onRestartWaitSecondsTextFieldChange.bind(this);
    }

    static isWaitSecondsValid(waitSeconds) {
        return waitSeconds.match(/^(0|[1-9][0-9]*)$/)
    }

    componentDidMount() {
        const { actions } = this.props;
        const { unsubscribeAll, subscribeContainers } = actions;
        unsubscribeAll();
        subscribeContainers();
    }

    componentDidUpdate(prevProps) {
        const { errors } = this.props;

        if (errors.length !== prevProps.errors.length)
            this.setState({
                errorDialogOpen: true
            })
    }

    openKillContainerDialog(containerId) {
        this.setState({
            dialogContainerId: containerId,
            killContainerDialogOpen: true
        });
    }

    openStopContainerDialog(containerId) {
        this.setState({
            dialogContainerId: containerId,
            stopContainerDialogOpen: true,
        })
    }

    openPauseContainerDialog(containerId) {
        this.setState({
            dialogContainerId: containerId,
            pauseContainerDialogOpen: true,
        })
    }

    openUnpauseContainerDialog(containerId) {
        this.setState({
            dialogContainerId: containerId,
            unpauseContainerDialogOpen: true,
        })
    }

    openStartContainerDialog(containerId) {
        this.setState({
            dialogContainerId: containerId,
            startContainerDialogOpen: true,
        })
    }

    openRestartContainerDialog(containerId) {
        this.setState({
            dialogContainerId: containerId,
            restartContainerDialogOpen: true,
        })
    }

    closeKillContainerDialog(event) {
        const { dialogContainerId, killSignal } = this.state;

        this.setState({
            killContainerDialogOpen: false,
            dialogContainerId: '',
            killSignal: ''
        });

        if (event.detail.action === 'accept')
            this.killContainer(dialogContainerId, killSignal);
    }

    closeStopContainerDialog(event) {
        const { dialogContainerId, stopWaitSeconds } = this.state;

        this.setState({
            stopContainerDialogOpen: false,
            dialogContainerId: '',
            stopWaitSeconds: '10'
        });

        if (event.detail.action === 'accept')
            this.stopContainer(dialogContainerId, stopWaitSeconds);
    }

    closePauseContainerDialog(event) {
        const { dialogContainerId } = this.state;

        this.setState({
            pauseContainerDialogOpen: false,
            dialogContainerId: ''
        });

        if (event.detail.action === 'accept')
            this.pauseContainer(dialogContainerId);
    }

    closeUnpauseContainerDialog(event) {
        const { dialogContainerId } = this.state;

        this.setState({
            unpauseContainerDialogOpen: false,
            dialogContainerId: ''
        });

        if (event.detail.action === 'accept')
            this.unpauseContainer(dialogContainerId);
    }

    closeStartContainerDialog(event) {
        const { dialogContainerId } = this.state;

        this.setState({
            startContainerDialogOpen: false,
            dialogContainerId: ''
        });

        if (event.detail.action === 'accept')
            this.startContainer(dialogContainerId);
    }

    closeRestartContainerDialog(event) {
        const { dialogContainerId, restartWaitSeconds } = this.state;

        this.setState({
            restartContainerDialogOpen: false,
            dialogContainerId: '',
            restartWaitSeconds: '10'
        });

        if (event.detail.action === 'accept')
            this.restartContainer(dialogContainerId, restartWaitSeconds);
    }

    closeErrorDialog(event) {
        this.setState({
            errorDialogOpen: false
        });
    }

    killContainer(containerId, killSignal) {
        const { actions } = this.props;
        actions.pushToMessageQueue(
            constants.container.CONTAINER_KILL,
            JSON.stringify({ containerId, killSignal })
        );
    }

    stopContainer(containerId, stopWaitSeconds) {
        const { actions } = this.props;
        actions.pushToMessageQueue(
            constants.container.CONTAINER_STOP,
            JSON.stringify({ containerId, waitSeconds: stopWaitSeconds })
        );
    }

    pauseContainer(containerId) {
        const { actions } = this.props;
        actions.pushToMessageQueue(
            constants.container.CONTAINER_PAUSE,
            JSON.stringify({ containerId })
        );
    }

    unpauseContainer(containerId) {
        const { actions } = this.props;
        actions.pushToMessageQueue(
            constants.container.CONTAINER_UNPAUSE,
            JSON.stringify({ containerId })
        );
    }

    startContainer(containerId) {
        const { actions } = this.props;
        actions.pushToMessageQueue(
            constants.container.CONTAINER_START,
            JSON.stringify({ containerId })
        );
    }

    restartContainer(containerId, restartWaitSeconds) {
        const { actions } = this.props;
        actions.pushToMessageQueue(
            constants.container.CONTAINER_RESTART,
            JSON.stringify({ containerId, waitSeconds: restartWaitSeconds })
        );
    }

    onKillSignalMenuOptionChange(event) {
        this.setState({
            killSignal: event.target.value
        });
    }

    onStopWaitSecondsTextFieldChange(event) {
        this.setState({
            stopWaitSeconds: event.target.value
        });
    }

    onRestartWaitSecondsTextFieldChange(event) {
        this.setState({
            restartWaitSeconds: event.target.value
        });
    }

    render() {
        const { containers, connected, errors, signals, nameAndIdFilter, pinnedContainerIds } = this.props;
        const { killContainerDialogOpen, stopContainerDialogOpen, pauseContainerDialogOpen, unpauseContainerDialogOpen, startContainerDialogOpen, restartContainerDialogOpen, killSignal, errorDialogOpen, stopWaitSeconds, restartWaitSeconds } = this.state;
        const disableActions = !connected;
        const stopWaitSecondsValid = ContainersView.isWaitSecondsValid(stopWaitSeconds);
        const restartWaitSecondsValid = ContainersView.isWaitSecondsValid(restartWaitSeconds);
        const trimmedFilterText = nameAndIdFilter.trim();
        const pinnedContainers = pinnedContainerIds.map(id => containers.find(container => container.Id === id));
        const allContainersPinnedFirst = pinnedContainers.concat(containers.filter(container => !pinnedContainerIds.includes(container.Id)));

        return (
            <React.Fragment>
                <Grid>
                    <ContainerFiltererContainer />
                    {allContainersPinnedFirst.map(container =>
                        <GridCell
                            span={1}
                            phone={4}
                            tablet={4}
                            desktop={3}
                            key={container.Id}>
                            <ContainerCardContainer
                                filterText={trimmedFilterText}
                                disableActions={disableActions}
                                actionFunctions={{
                                    [constants.container.ACTIONS.PAUSE]: this.openPauseContainerDialog,
                                    [constants.container.ACTIONS.STOP]: this.openStopContainerDialog,
                                    [constants.container.ACTIONS.KILL]: this.openKillContainerDialog,
                                    [constants.container.ACTIONS.UNPAUSE]: this.openUnpauseContainerDialog,
                                    [constants.container.ACTIONS.START]: this.openStartContainerDialog,
                                    [constants.container.ACTIONS.RESTART]: this.openRestartContainerDialog
                                }}
                                container={container} />
                        </GridCell>
                    )}
                </Grid>
                <ContainerActionDialog
                    open={killContainerDialogOpen}
                    onClose={this.closeKillContainerDialog}
                    title={'Kill Container'}
                    positiveActionDisabled={disableActions}>
                    <Select
                        value={killSignal}
                        onChange={this.onKillSignalMenuOptionChange}
                        label="Signal"
                        placeholder="Default"
                        options={signals} />
                </ContainerActionDialog>
                <ContainerActionDialog
                    open={stopContainerDialogOpen}
                    onClose={this.closeStopContainerDialog}
                    title={'Stop Container'}
                    positiveActionDisabled={disableActions || !stopWaitSecondsValid}>
                    <TextField
                        invalid={!stopWaitSecondsValid}
                        label="Seconds to wait"
                        value={stopWaitSeconds}
                        onChange={this.onStopWaitSecondsTextFieldChange} />
                </ContainerActionDialog>
                <ContainerActionDialog
                    open={pauseContainerDialogOpen}
                    onClose={this.closePauseContainerDialog}
                    title={'Pause Container'}
                    positiveActionDisabled={disableActions} />
                <ContainerActionDialog
                    open={unpauseContainerDialogOpen}
                    onClose={this.closeUnpauseContainerDialog}
                    title={'Unpause Container'}
                    positiveActionDisabled={disableActions} />
                <ContainerActionDialog
                    open={startContainerDialogOpen}
                    onClose={this.closeStartContainerDialog}
                    title={'Start Container'}
                    positiveActionDisabled={disableActions} />
                <ContainerActionDialog
                    open={restartContainerDialogOpen}
                    onClose={this.closeRestartContainerDialog}
                    title={'Restart Container'}
                    positiveActionDisabled={disableActions || !restartWaitSecondsValid}>
                    <TextField
                        invalid={!restartWaitSecondsValid}
                        label="Seconds to wait"
                        value={restartWaitSeconds}
                        onChange={this.onRestartWaitSecondsTextFieldChange} />
                </ContainerActionDialog>
                <Dialog
                    open={errorDialogOpen}
                    onClose={this.closeErrorDialog}>
                    <DialogTitle theme="textPrimaryOnBackground">
                        Error
                    </DialogTitle>
                    <DialogContent theme="textPrimaryOnBackground">
                        {errors[errors.length - 1]}
                    </DialogContent>
                    <DialogActions>
                        <DialogButton
                            action="accept"
                            isDefaultAction>
                            Dismiss
                        </DialogButton>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        );
    }
}

ContainersView.propTypes = {
    containers: PropTypes.arrayOf(PropTypes.shape({
        Id: PropTypes.string,
        Names: PropTypes.arrayOf(PropTypes.string),
        Image: PropTypes.string,
        ImageID: PropTypes.string,
        Command: PropTypes.string,
        Created: PropTypes.number,
        State: PropTypes.string,
        Status: PropTypes.string,
        Ports: PropTypes.arrayOf(PropTypes.shape({
            PrivatePort: PropTypes.number,
            PublicPort: PropTypes.number,
            Type: PropTypes.string,
            IP: PropTypes.string
        })),
        Labels: PropTypes.objectOf(PropTypes.string),
        SizeRw: PropTypes.number,
        SizeRootFs: PropTypes.number,
        NetworkSettings: PropTypes.shape({
            IPAddress: PropTypes.string,
            IPPrefixLen: PropTypes.number,
            Gateway: PropTypes.string,
            Bridge: PropTypes.string,
            PortMapping: PropTypes.objectOf(PropTypes.objectOf(PropTypes.string)),
            Ports: PropTypes.objectOf(PropTypes.shape({
                HostIp: PropTypes.string,
                HostPort: PropTypes.string
            })),
            MacAddress: PropTypes.string,
            Networks: PropTypes.objectOf(PropTypes.shape({
                Aliases: PropTypes.arrayOf(PropTypes.string),
                NetworkID: PropTypes.string,
                EndpointID: PropTypes.string,
                Gateway: PropTypes.string,
                IPAddress: PropTypes.string,
                IPPrefixLen: PropTypes.number,
                IPv6Gateway: PropTypes.string,
                GlobalIPv6Address: PropTypes.string,
                GlobalIPv6PrefixLen: PropTypes.number,
                MacAddress: PropTypes.string
            })),
            EndpointID: PropTypes.string,
            SandboxID: PropTypes.string,
            SandboxKey: PropTypes.string,
            HairpinMode: PropTypes.bool,
            LinkLocalIPv6Address: PropTypes.string,
            LinkLocalIPv6PrefixLen: PropTypes.number,
            GlobalIPv6Address: PropTypes.string,
            GlobalIPv6PrefixLen: PropTypes.number,
            IPv6Gateway: PropTypes.string
        }),
        Mounts: PropTypes.arrayOf(PropTypes.shape({
            Type: PropTypes.string,
            Name: PropTypes.string,
            Source: PropTypes.string,
            Destination: PropTypes.string,
            Driver: PropTypes.string,
            Mode: PropTypes.string,
            RW: PropTypes.bool,
            Propagation: PropTypes.string
        }))
    })).isRequired,
    connected: PropTypes.bool.isRequired,
    errors: PropTypes.arrayOf(PropTypes.string).isRequired,
    signals: PropTypes.arrayOf(PropTypes.string).isRequired,
    nameAndIdFilter: PropTypes.string.isRequired,
    actions: PropTypes.shape({
        pushToMessageQueue: PropTypes.func.isRequired,
        unsubscribeAll: PropTypes.func.isRequired,
        subscribeContainers: PropTypes.func.isRequired
    }).isRequired,
    pinnedContainerIds: PropTypes.arrayOf(PropTypes.string).isRequired
};

export default ContainersView;