import React, { Component } from 'react';
import { Typography } from '@rmwc/typography';
import '@material/typography/dist/mdc.typography.css';
import { List, ListDivider, SimpleListItem } from '@rmwc/list';
import '@material/list/dist/mdc.list.css';
import { Card } from '@rmwc/card';
import '@material/card/dist/mdc.card.css';
import '@material/button/dist/mdc.button.css';
import '@material/icon-button/dist/mdc.icon-button.css';
import PropTypes from 'prop-types';

class ContainerLabelsCard extends Component {

    render() {
        const { Labels } = this.props;

        return (
            <Card>
                <div style={{ padding: '0 1rem 0 1rem' }}>
                    <Typography
                        use="subtitle1"
                        tag="h2"
                        style={{
                            textOverflow: 'ellipsis',
                            whiteSpace: 'nowrap',
                            overflow: 'hidden'
                        }}>
                        Labels
                    </Typography>
                </div>
                <ListDivider />
                <List
                    twoLine
                    dense
                    nonInteractive>
                    {Object.keys(Labels)
                        .map(labelKey =>
                            <SimpleListItem
                                ripple={false}
                                text={labelKey}
                                secondaryText={Labels[labelKey]}
                                key={labelKey} />
                        )}
                </List>
            </Card >
        );
    }
}

ContainerLabelsCard.propTypes = {
    Labels: PropTypes.objectOf(PropTypes.string).isRequired
};

export default ContainerLabelsCard;