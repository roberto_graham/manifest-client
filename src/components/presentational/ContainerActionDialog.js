import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dialog, DialogActions, DialogButton, DialogContent, DialogTitle } from '@rmwc/dialog';
import '@material/dialog/dist/mdc.dialog.css';
import '@material/button/dist/mdc.button.css';

class ContainerActionDialog extends Component {

    render() {
        const { positiveActionDisabled, title, open, onClose, children } = this.props;

        return (
            <Dialog
                open={open}
                onClose={onClose}>
                <DialogTitle theme="textPrimaryOnBackground">
                    {title}
                </DialogTitle>
                <DialogContent>
                    {children ?
                        children
                        : <React.Fragment />}
                </DialogContent>
                <DialogActions>
                    <DialogButton action="close">
                        Cancel
                    </DialogButton>
                    <DialogButton
                        disabled={positiveActionDisabled}
                        action="accept"
                        isDefaultAction>
                        Confirm
                    </DialogButton>
                </DialogActions>
            </Dialog>
        );
    }
}

ContainerActionDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    positiveActionDisabled: PropTypes.bool.isRequired,
    children: PropTypes.any
};

export default ContainerActionDialog;