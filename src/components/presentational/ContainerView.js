import React, { Component } from 'react';
import PropTypes from 'prop-types';
import constants from '../../constants';
import { Grid, GridCell } from '@rmwc/grid';
import '@material/layout-grid/dist/mdc.layout-grid.css';
import { Card } from '@rmwc/card';
import '@material/card/dist/mdc.card.css';
import '@material/button/dist/mdc.button.css';
import '@material/icon-button/dist/mdc.icon-button.css';
import { List, ListDivider, SimpleListItem } from '@rmwc/list';
import '@material/list/dist/mdc.list.css';
import { Typography } from '@rmwc/typography';
import '@material/typography/dist/mdc.typography.css';
import { Charts, ChartContainer, ChartRow, YAxis, LineChart, Resizable, EventMarker } from 'react-timeseries-charts';
import { TimeSeries, TimeEvent } from "pondjs";
import byteSize from 'byte-size'
import ContainerLabelsCard from './ContainerLabelsCard';
import ContainerImageCard from './ContainerImageCard';
import { Dialog, DialogActions, DialogButton, DialogContent, DialogTitle } from '@rmwc/dialog';
import '@material/dialog/dist/mdc.dialog.css';
import '@material/button/dist/mdc.button.css';

class ContainerView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            statistics: [],
            events: [],
            tracker: null,
            memoryUsageTrackerValue: '0 B',
            cpuUsageTrackerValue: '0%',
            trackerEvent: null,
            errorDialogOpen: false
        };
        this.handleTrackerChanged = this.handleTrackerChanged.bind(this);
        this.createTimeEventFromStatistics = this.createTimeEventFromStatistics.bind(this);
        this.fetchImageData = this.fetchImageData.bind(this);
        this.closeErrorDialog = this.closeErrorDialog.bind(this);
    }

    componentDidMount() {
        const { actions, container, statistics } = this.props;
        const { unsubscribeAll, subscribeContainers, subscribeContainerStatistics } = actions;
        unsubscribeAll();
        subscribeContainers();
        if (container) {
            subscribeContainerStatistics(container.Id);
            this.fetchImageData();
        }
        if (statistics)
            this.setState({
                events: [this.createTimeEventFromStatistics(statistics)]
            });
    }

    componentDidUpdate(prevProps) {
        const { actions, container, statistics, errors } = this.props;
        const { events } = this.state;
        const { subscribeContainerStatistics } = actions;
        if (container && !prevProps.container) {
            subscribeContainerStatistics(container.Id);
            this.fetchImageData();
        }
        if (statistics && (events.length === 0 || events[events.length - 1].timestamp() < new Date(statistics.read)))
            this.setState({
                events: [...events, this.createTimeEventFromStatistics(statistics)]
            });
        if (errors.length !== prevProps.errors.length)
            this.setState({
                errorDialogOpen: true
            })
    }

    closeErrorDialog(event) {
        this.setState({
            errorDialogOpen: false
        });
    }

    fetchImageData() {
        const { container, actions } = this.props;
        if (container) {
            const { ImageID } = container;
            if (ImageID) {
                actions.subscribeImageInfo(ImageID);
                actions.pushToMessageQueue(
                    `${constants.image.IMAGE}/${ImageID}/info`,
                    JSON.stringify({})
                );
            }
        }
    }

    createTimeEventFromStatistics(statistics) {
        return new TimeEvent(new Date(statistics.read), {
            memoryUsage: this.getMemoryUsageFromStatistics(statistics),
            cpuUsage: this.getCpuUsageFromStatistics(statistics)
        });
    }

    getMemoryUsageFromStatistics(statistics) {
        return statistics.memory_stats ? statistics.memory_stats.usage : 0;
    }

    getCpuUsageFromStatistics(statistics) {
        let cpuUsageAsPercent = 0;
        const { precpu_stats, cpu_stats } = statistics;
        if (precpu_stats && cpu_stats) {
            const previousTotalCpuUsage = precpu_stats.cpu_usage.total_usage;
            const previousSystemCpuUsage = precpu_stats.system_cpu_usage;
            const totalCpuUsageDifference = cpu_stats.cpu_usage.total_usage - previousTotalCpuUsage;
            const systemCpuUsageDifference = cpu_stats.system_cpu_usage - previousSystemCpuUsage;
            if (systemCpuUsageDifference > 0 && totalCpuUsageDifference > 0) {
                const numberOfCpus = cpu_stats.cpu_usage.percpu_usage.filter(perCpuUsage => perCpuUsage !== 0).length;
                cpuUsageAsPercent = ((totalCpuUsageDifference / systemCpuUsageDifference) * numberOfCpus) * 100.0;
            }
        }
        return cpuUsageAsPercent;
    }

    handleTrackerChanged(time, timeSeries) {
        if (time) {
            const timeEvent = timeSeries.atTime(time);
            this.setState({
                tracker: timeEvent.timestamp(),
                memoryUsageTrackerValue: this.formatBytes(timeEvent.toPoint(['memoryUsage'])[1]),
                cpuUsageTrackerValue: this.formatPercentage(timeEvent.toPoint(['cpuUsage'])[1]),
                trackerEvent: timeEvent
            });
        } else
            this.setState({
                tracker: null,
                memoryUsageTrackerValue: null,
                cpuUsageTrackerValue: null,
                trackerEvent: null
            });
    }

    formatBytes(bytes) {
        const result = byteSize(bytes, { precision: 3, units: 'iec' });
        return `${result.value} ${result.unit}`;
    }

    formatPercentage(percentage) {
        return `${percentage.toFixed(2)}%`
    }

    render() {
        const { container, errors } = this.props;
        const { events, tracker, memoryUsageTrackerValue, cpuUsageTrackerValue, trackerEvent, errorDialogOpen } = this.state;
        const statsSeries = new TimeSeries({
            name: "stats",
            events: events
        });

        return (
            <React.Fragment>
                <Grid>
                    {container ?
                        <React.Fragment>
                            <GridCell
                                span={1}
                                phone={4}
                                tablet={4}
                                desktop={3}>
                                <ContainerLabelsCard Labels={container.Labels} />
                            </GridCell>
                            <GridCell
                                span={1}
                                phone={4}
                                tablet={4}
                                desktop={3}>
                                <ContainerImageCard Image={container.Image} ImageID={container.ImageID} />
                            </GridCell>
                            <GridCell
                                span={1}
                                phone={4}
                                tablet={8}
                                desktop={12}>
                                <Card>
                                    <div style={{ padding: '0 1rem 0 1rem' }}>
                                        <Typography
                                            use="subtitle1"
                                            tag="h2"
                                            style={{
                                                textOverflow: 'ellipsis',
                                                whiteSpace: 'nowrap',
                                                overflow: 'hidden'
                                            }}>
                                            Memory Usage
                                    </Typography>
                                    </div>
                                    {events.length > 1 ?
                                        <div style={{ paddingBottom: '1rem' }}>
                                            <Resizable>
                                                <ChartContainer
                                                    trackerPosition={tracker}
                                                    onTrackerChanged={(time) => this.handleTrackerChanged(time, statsSeries)}
                                                    timeRange={statsSeries.timerange()}>
                                                    <ChartRow>
                                                        <YAxis
                                                            id="axis1"
                                                            min={0}
                                                            max={statsSeries.max('memoryUsage')}
                                                            format={usage => this.formatBytes(usage)}
                                                            type="linear" />
                                                        <Charts>
                                                            <LineChart
                                                                axis="axis1"
                                                                series={statsSeries}
                                                                columns={['memoryUsage']}
                                                                interpolation="curveLinear" />
                                                            {tracker ?
                                                                <EventMarker
                                                                    type="flag"
                                                                    axis="axis1"
                                                                    event={trackerEvent}
                                                                    column="memoryUsage"
                                                                    info={[{ label: 'Memory', value: memoryUsageTrackerValue }]}
                                                                    infoTimeFormat="%d/%m/%y %-I:%M:%S %p"
                                                                    infoWidth={120}
                                                                    markerRadius={2}
                                                                    markerStyle={{ fill: "black" }} />
                                                                : <g />}
                                                        </Charts>
                                                    </ChartRow>
                                                </ChartContainer>
                                            </Resizable>
                                        </div>
                                        : <React.Fragment />}
                                </Card>
                            </GridCell>
                            <GridCell
                                span={1}
                                phone={4}
                                tablet={8}
                                desktop={12}>
                                <Card>
                                    <div style={{ padding: '0 1rem 0 1rem' }}>
                                        <Typography
                                            use="subtitle1"
                                            tag="h2"
                                            style={{
                                                textOverflow: 'ellipsis',
                                                whiteSpace: 'nowrap',
                                                overflow: 'hidden'
                                            }}>
                                            CPU Usage
                                    </Typography>
                                    </div>
                                    {events.length > 1 ?
                                        <div style={{ paddingBottom: '1rem' }}>
                                            <Resizable>
                                                <ChartContainer
                                                    trackerPosition={tracker}
                                                    onTrackerChanged={(time) => this.handleTrackerChanged(time, statsSeries)}
                                                    timeRange={statsSeries.timerange()}>
                                                    <ChartRow>
                                                        <YAxis
                                                            id="axis1"
                                                            min={0}
                                                            max={statsSeries.max('cpuUsage')}
                                                            format={usage => this.formatPercentage(usage)}
                                                            type="linear" />
                                                        <Charts>
                                                            <LineChart
                                                                axis="axis1"
                                                                series={statsSeries}
                                                                columns={['cpuUsage']}
                                                                interpolation="curveLinear" />
                                                            {tracker ?
                                                                <EventMarker
                                                                    type="flag"
                                                                    axis="axis1"
                                                                    event={trackerEvent}
                                                                    column="memoryUsage"
                                                                    info={[{ label: 'CPU', value: cpuUsageTrackerValue }]}
                                                                    infoTimeFormat="%d/%m/%y %-I:%M:%S %p"
                                                                    infoWidth={120}
                                                                    markerRadius={2}
                                                                    markerStyle={{ fill: "black" }} />
                                                                : <g />}
                                                        </Charts>
                                                    </ChartRow>
                                                </ChartContainer>
                                            </Resizable>
                                        </div>
                                        : <React.Fragment />}
                                </Card>
                            </GridCell>
                        </React.Fragment>
                        : <GridCell span={12}>
                            <Typography
                                use="subtitle1"
                                tag="h2"
                                theme="textPrimaryOnBackground"
                                style={{ margin: '0' }}>
                                Container not found
                            </Typography>
                        </GridCell>}
                </Grid>
                <Dialog
                    open={errorDialogOpen}
                    onClose={this.closeErrorDialog}>
                    <DialogTitle theme="textPrimaryOnBackground">
                        Error
                    </DialogTitle>
                    <DialogContent theme="textPrimaryOnBackground">
                        {errors[errors.length - 1]}
                    </DialogContent>
                    <DialogActions>
                        <DialogButton
                            action="accept"
                            isDefaultAction>
                            Dismiss
                        </DialogButton>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        );
    }
}

ContainerView.propTypes = {
    errors: PropTypes.arrayOf(PropTypes.string).isRequired,
    container: PropTypes.shape({
        Id: PropTypes.string,
        Names: PropTypes.arrayOf(PropTypes.string),
        Image: PropTypes.string,
        ImageID: PropTypes.string,
        Command: PropTypes.string,
        Created: PropTypes.number,
        State: PropTypes.string,
        Status: PropTypes.string,
        Ports: PropTypes.arrayOf(PropTypes.shape({
            PrivatePort: PropTypes.number,
            PublicPort: PropTypes.number,
            Type: PropTypes.string,
            IP: PropTypes.string
        })),
        Labels: PropTypes.objectOf(PropTypes.string),
        SizeRw: PropTypes.number,
        SizeRootFs: PropTypes.number,
        NetworkSettings: PropTypes.shape({
            IPAddress: PropTypes.string,
            IPPrefixLen: PropTypes.number,
            Gateway: PropTypes.string,
            Bridge: PropTypes.string,
            PortMapping: PropTypes.objectOf(PropTypes.objectOf(PropTypes.string)),
            Ports: PropTypes.objectOf(PropTypes.shape({
                HostIp: PropTypes.string,
                HostPort: PropTypes.string
            })),
            MacAddress: PropTypes.string,
            Networks: PropTypes.objectOf(PropTypes.shape({
                Aliases: PropTypes.arrayOf(PropTypes.string),
                NetworkID: PropTypes.string,
                EndpointID: PropTypes.string,
                Gateway: PropTypes.string,
                IPAddress: PropTypes.string,
                IPPrefixLen: PropTypes.number,
                IPv6Gateway: PropTypes.string,
                GlobalIPv6Address: PropTypes.string,
                GlobalIPv6PrefixLen: PropTypes.number,
                MacAddress: PropTypes.string
            })),
            EndpointID: PropTypes.string,
            SandboxID: PropTypes.string,
            SandboxKey: PropTypes.string,
            HairpinMode: PropTypes.bool,
            LinkLocalIPv6Address: PropTypes.string,
            LinkLocalIPv6PrefixLen: PropTypes.number,
            GlobalIPv6Address: PropTypes.string,
            GlobalIPv6PrefixLen: PropTypes.number,
            IPv6Gateway: PropTypes.string
        }),
        Mounts: PropTypes.arrayOf(PropTypes.shape({
            Type: PropTypes.string,
            Name: PropTypes.string,
            Source: PropTypes.string,
            Destination: PropTypes.string,
            Driver: PropTypes.string,
            Mode: PropTypes.string,
            RW: PropTypes.bool,
            Propagation: PropTypes.string
        }))
    }),
    actions: PropTypes.shape({
        pushToMessageQueue: PropTypes.func.isRequired,
        unsubscribeAll: PropTypes.func.isRequired,
        subscribeContainers: PropTypes.func.isRequired,
        subscribeImageInfo: PropTypes.func.isRequired,
        subscribeContainerStatistics: PropTypes.func.isRequired,
    }).isRequired,
    statistics: PropTypes.object
};

export default ContainerView;