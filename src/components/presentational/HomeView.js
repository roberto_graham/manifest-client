import React, { Component } from 'react';
import { Grid, GridCell } from '@rmwc/grid';
import '@material/layout-grid/dist/mdc.layout-grid.css';
import ShowViewActionCard from './ShowViewActionCard';
import PropTypes from 'prop-types';

class HomeView extends Component {

    componentDidMount() {
        const { actions } = this.props;
        const { unsubscribeAll, subscribeContainers } = actions;
        unsubscribeAll();
        subscribeContainers();
    }

    render() {
        const { containerCount } = this.props;

        return (
            <Grid>
                <GridCell
                    span={1}
                    phone={4}
                    tablet={4}
                    desktop={3}>
                    <ShowViewActionCard
                        title={"Containers"}
                        link={"/containers/"}
                        count={containerCount} />
                </GridCell>
            </Grid>
        );
    }
}

HomeView.propTypes = {
    containerCount: PropTypes.number.isRequired,
    actions: PropTypes.shape({
        unsubscribeAll: PropTypes.func.isRequired,
        subscribeContainers: PropTypes.func.isRequired
    }).isRequired
};

export default HomeView;