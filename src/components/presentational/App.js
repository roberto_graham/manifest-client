import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import ContainersViewContainer from '../containers/ContainersViewContainer';
import PropTypes from 'prop-types';
import { ThemeProvider } from '@rmwc/theme';
import '@material/theme/dist/mdc.theme.css';
import HomeViewContainer from '../containers/HomeViewContainer';
import StompClientContainer from "../containers/StompClientContainer";
import AppTitleContainer from "../containers/AppTitleContainer";
import ContainerViewContainer from '../containers/ContainerViewContainer';

class App extends Component {

    static Home() {
        return <HomeViewContainer />
    }

    static Containers() {
        return <ContainersViewContainer />
    }

    static Container({ match }) {
        const { params } = match;
        const { id } = params;

        return <ContainerViewContainer id={id} />
    }

    static NoMatch({ location }) {
        const { pathname } = location;

        return <p>{`No match for ${pathname}`}</p>;
    }

    render() {
        const { connected } = this.props;

        return (
            <React.Fragment>
                <StompClientContainer />
                <ThemeProvider options={themeOptions}>
                    <AppTitleContainer connected={connected} />
                    <Switch>
                        <Route
                            path="/"
                            exact
                            component={App.Home} />
                        <Route
                            path="/containers/"
                            exact
                            component={App.Containers} />
                        <Route
                            path="/containers/:id([a-z0-9]+)/"
                            exact
                            component={App.Container} />
                        <Route component={App.NoMatch} />
                    </Switch>
                </ThemeProvider>
            </React.Fragment>
        );
    }
}

const themeOptions = {
    primary: '#303030',
    secondary: '#661fff',
    error: '#b00020',
    background: '#fff',
    surface: '#fff',
    onPrimary: 'rgba(255, 255, 255, 1)',
    onSecondary: 'rgba(255, 255, 255, 1)',
    onSurface: 'rgba(0, 0, 0, 0.87)',
    onError: '#fff',
    textPrimaryOnBackground: 'rgba(0, 0, 0, 0.87)',
    textSecondaryOnBackground: 'rgba(0, 0, 0, 0.54)',
    textHintOnBackground: 'rgba(0, 0, 0, 0.38)',
    textDisabledOnBackground: 'rgba(0, 0, 0, 0.38)',
    textIconOnBackground: 'rgba(0, 0, 0, 0.38)',
    textPrimaryOnLight: 'rgba(0, 0, 0, 0.87)',
    textSecondaryOnLight: 'rgba(0, 0, 0, 0.54)',
    textHintOnLight: 'rgba(0, 0, 0, 0.38)',
    textDisabledOnLight: 'rgba(0, 0, 0, 0.38)',
    textIconOnLight: 'rgba(0, 0, 0, 0.38)',
    textPrimaryOnDark: 'white',
    textSecondaryOnDark: 'rgba(255, 255, 255, 0.7)',
    textHintOnDark: 'rgba(255, 255, 255, 0.5)',
    textDisabledOnDark: 'rgba(255, 255, 255, 0.5)',
    textIconOnDark: 'rgba(255, 255, 255, 0.5)'
};

App.propTypes = {
    connected: PropTypes.bool.isRequired
};

export default App;
