import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import actions from "../../actions";
import HomeView from '../presentational/HomeView';
import selectors from '../../selectors';

const mapStateToProps = (store) => {
    return {
        containerCount: selectors.selectContainerCount(store)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators({
            unsubscribeAll: actions.socket.unsubscribeAll,
            subscribeContainers: actions.socket.subscribeContainers
        }, dispatch)
    };
};

class HomeViewContainer extends Component {

    render() {
        return (
            <HomeView {...this.props} />
        );
    }
}

HomeViewContainer.propTypes = {
    containerCount: PropTypes.number.isRequired,
    actions: PropTypes.shape({
        unsubscribeAll: PropTypes.func.isRequired,
        subscribeContainers: PropTypes.func.isRequired
    }).isRequired
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HomeViewContainer));
