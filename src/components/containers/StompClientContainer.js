import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import actions from '../../actions';
import { bindActionCreators } from 'redux'
import StompClient from "../presentational/StompClient";

const mapStateToProps = (store) => {
    return {
        topics: store.socket.topics,
        messageQueue: store.socket.messageQueue,
        connected: store.socket.connected
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators({
            receiveMessage: actions.socket.receiveMessage,
            updateConnectedStatus: actions.socket.updateConnectedStatus,
            shiftMessageQueue: actions.socket.shiftMessageQueue
        }, dispatch)
    };
};

class StompClientContainer extends Component {

    render() {
        return (
            <StompClient {...this.props} />
        );
    }
}

StompClientContainer.propTypes = {
    topics: PropTypes.arrayOf(PropTypes.string).isRequired,
    actions: PropTypes.shape({
        receiveMessage: PropTypes.func.isRequired,
        updateConnectedStatus: PropTypes.func.isRequired,
        shiftMessageQueue: PropTypes.func.isRequired
    }).isRequired,
    messageQueue: PropTypes.arrayOf(PropTypes.shape({
        topic: PropTypes.string,
        data: PropTypes.any
    })),
    connected: PropTypes.bool.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(StompClientContainer);
