import React, { Component } from 'react'
import ContainerFilterer from "../presentational/ContainerFilterer";
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import actions from '../../actions';
import { bindActionCreators } from 'redux'
import selectors from '../../selectors'

const mapStateToProps = (store) => {
    return {
        nameAndIdFilter: selectors.selectContainerNameAndIdFilter(store),
        states: selectors.selectAllContainerStates(store),
        filteredOutStates: selectors.selectAllFilteredOutContainerStates(store),
        stateCounts: selectors.selectContainerStateCounts(store)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators({
            updateNameAndIdFilter: actions.container.updateNameAndIdFilter,
            turnStateOff: actions.container.turnStateOff,
            turnStateOn: actions.container.turnStateOn
        }, dispatch)
    };
};

class ContainerFiltererContainer extends Component {

    render() {
        return (
            <ContainerFilterer {...this.props} />
        );
    }
}

ContainerFiltererContainer.propTypes = {
    nameAndIdFilter: PropTypes.string.isRequired,
    states: PropTypes.arrayOf(PropTypes.string).isRequired,
    filteredOutStates: PropTypes.arrayOf(PropTypes.string).isRequired,
    actions: PropTypes.shape({
        updateNameAndIdFilter: PropTypes.func.isRequired,
        turnStateOn: PropTypes.func.isRequired,
        turnStateOff: PropTypes.func.isRequired
    }).isRequired,
    stateCounts: PropTypes.objectOf(PropTypes.number).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(ContainerFiltererContainer);