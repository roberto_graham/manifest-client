import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import actions from '../../actions';
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router-dom'
import selectors from '../../selectors';
import ContainerView from '../presentational/ContainerView';

const mapStateToProps = (store, props) => {
    const { id } = props;

    return {
        container: selectors.selectContainerById(id, store),
        statistics: selectors.selectContainerStatisticsById(id, store),
        errors: store.image.errors,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators({
            pushToMessageQueue: actions.socket.pushToMessageQueue,
            unsubscribeAll: actions.socket.unsubscribeAll,
            subscribeContainers: actions.socket.subscribeContainers,
            subscribeImageInfo: actions.socket.subscribeImageInfo,
            subscribeContainerStatistics: actions.socket.subscribeContainerStatistics,
        }, dispatch)
    };
};

class ContainerViewContainer extends Component {

    render() {
        return (
            <ContainerView {...this.props} />
        );
    }
}

ContainerViewContainer.propTypes = {
    id: PropTypes.string.isRequired,
    container: PropTypes.shape({
        Id: PropTypes.string,
        Names: PropTypes.arrayOf(PropTypes.string),
        Image: PropTypes.string,
        ImageID: PropTypes.string,
        Command: PropTypes.string,
        Created: PropTypes.number,
        State: PropTypes.string,
        Status: PropTypes.string,
        Ports: PropTypes.arrayOf(PropTypes.shape({
            PrivatePort: PropTypes.number,
            PublicPort: PropTypes.number,
            Type: PropTypes.string,
            IP: PropTypes.string
        })),
        Labels: PropTypes.objectOf(PropTypes.string),
        SizeRw: PropTypes.number,
        SizeRootFs: PropTypes.number,
        NetworkSettings: PropTypes.shape({
            IPAddress: PropTypes.string,
            IPPrefixLen: PropTypes.number,
            Gateway: PropTypes.string,
            Bridge: PropTypes.string,
            PortMapping: PropTypes.objectOf(PropTypes.objectOf(PropTypes.string)),
            Ports: PropTypes.objectOf(PropTypes.shape({
                HostIp: PropTypes.string,
                HostPort: PropTypes.string
            })),
            MacAddress: PropTypes.string,
            Networks: PropTypes.objectOf(PropTypes.shape({
                Aliases: PropTypes.arrayOf(PropTypes.string),
                NetworkID: PropTypes.string,
                EndpointID: PropTypes.string,
                Gateway: PropTypes.string,
                IPAddress: PropTypes.string,
                IPPrefixLen: PropTypes.number,
                IPv6Gateway: PropTypes.string,
                GlobalIPv6Address: PropTypes.string,
                GlobalIPv6PrefixLen: PropTypes.number,
                MacAddress: PropTypes.string
            })),
            EndpointID: PropTypes.string,
            SandboxID: PropTypes.string,
            SandboxKey: PropTypes.string,
            HairpinMode: PropTypes.bool,
            LinkLocalIPv6Address: PropTypes.string,
            LinkLocalIPv6PrefixLen: PropTypes.number,
            GlobalIPv6Address: PropTypes.string,
            GlobalIPv6PrefixLen: PropTypes.number,
            IPv6Gateway: PropTypes.string
        }),
        Mounts: PropTypes.arrayOf(PropTypes.shape({
            Type: PropTypes.string,
            Name: PropTypes.string,
            Source: PropTypes.string,
            Destination: PropTypes.string,
            Driver: PropTypes.string,
            Mode: PropTypes.string,
            RW: PropTypes.bool,
            Propagation: PropTypes.string
        }))
    }),
    errors: PropTypes.arrayOf(PropTypes.string).isRequired,
    actions: PropTypes.shape({
        pushToMessageQueue: PropTypes.func.isRequired,
        unsubscribeAll: PropTypes.func.isRequired,
        subscribeContainers: PropTypes.func.isRequired,
        subscribeImageInfo: PropTypes.func.isRequired,
        subscribeContainerStatistics: PropTypes.func.isRequired,
    }).isRequired,
    statistics: PropTypes.object
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ContainerViewContainer));
