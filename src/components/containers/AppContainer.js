import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import App from '../presentational/App';
import { withRouter } from 'react-router-dom';

const mapStateToProps = (store) => {
    return {
        connected: store.socket.connected,
    };
};

class AppContainer extends Component {

    render() {
        return (
            <App {...this.props} />
        );
    }
}

AppContainer.propTypes = {
    connected: PropTypes.bool.isRequired
};

export default withRouter(connect(mapStateToProps, null)(AppContainer));
