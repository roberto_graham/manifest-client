import React, { Component } from 'react'
import AppTitle from "../presentational/AppTitle";
import PropTypes from 'prop-types';
import { connect } from "react-redux"

const mapStateToProps = (store) => {
    return {
        connected: store.socket.connected,
        pathname: store.router.location.pathname
    }
};

class AppTitleContainer extends Component {

    render() {
        return (
            <AppTitle {...this.props} />
        );
    }
}

AppTitleContainer.propTypes = {
    connected: PropTypes.bool.isRequired,
    pathname: PropTypes.string.isRequired,
};

export default connect(mapStateToProps, null)(AppTitleContainer);