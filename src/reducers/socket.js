import constants from '../constants';

const defaultState = {
    topics: [],
    connected: false,
    messageQueue: []
};

const socket = (state = defaultState, action) => {
    switch (action.type) {
        case constants.socket.SUBSCRIBE_CONTAINERS:
            return {
                ...state,
                topics: [...new Set([
                    ...state.topics,
                    constants.container.CONTAINER_TOPIC_UPDATE,
                    constants.container.CONTAINER_QUEUE_UPDATE,
                    constants.container.CONTAINER_QUEUE_UPDATE_ERROR
                ])]
            };
        case constants.socket.SUBSCRIBE_CONTAINER_STATISTICS: {
            return {
                ...state,
                topics: [...new Set([
                    ...state.topics,
                    `${constants.container.CONTAINER_QUEUE_UPDATE}/${action.data}/statistics`,
                    `${constants.container.CONTAINER_TOPIC_UPDATE}/${action.data}/statistics`
                ])]
            }
        }
        case constants.socket.SUBSCRIBE_IMAGE_INFO: {
            return {
                ...state,
                topics: [...new Set([
                    ...state.topics,
                    `${constants.image.IMAGE_QUEUE_UPDATE}/${action.data}`,
                    constants.image.IMAGE_QUEUE_UPDATE_ERROR
                ])]
            }
        }
        case constants.socket.UNSUBSCRIBE_ALL:
            return {
                ...state,
                topics: []
            };
        case constants.socket.UPDATE_CONNECTED_STATUS:
            return {
                ...state,
                connected: action.data
            };
        case constants.socket.PUSH_TO_MESSAGE_QUEUE:
            return {
                ...state,
                messageQueue: [...state.messageQueue, action.data]
            };
        case constants.socket.SHIFT_MESSAGE_QUEUE:
            return {
                ...state,
                messageQueue: [...state.messageQueue.slice(1)]
            };
        default:
            return state;
    }
};

export default socket;
