import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import socket from './socket';
import container from './container';
import image from './image';

export default (history) => combineReducers({
    router: connectRouter(history),
    socket,
    container,
    image
});
