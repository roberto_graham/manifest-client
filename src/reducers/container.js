import constants from '../constants';

const defaultState = {
    containers: [],
    signals: [],
    errors: [],
    statistics: {},
    nameAndIdFilter: '',
    filteredOutStates: [],
    pinnedContainerIds: []
};

const container = (state = defaultState, action) => {
    const containerId = state.containers.map(container => container.Id)
        .find(containerId => action.type === `${constants.container.CONTAINER_QUEUE_UPDATE}/${containerId}/statistics`
            || action.type === `${constants.container.CONTAINER_TOPIC_UPDATE}/${containerId}/statistics`);
    if (containerId)
        return {
            ...state,
            statistics: {
                ...state.statistics,
                [containerId]: action.data
            }
        }
    switch (action.type) {
        case constants.container.CONTAINER_QUEUE_UPDATE:
            return {
                ...state,
                containers: action.data.containers,
                signals: action.data.killSignals
            };
        case constants.container.CONTAINER_TOPIC_UPDATE:
            return {
                ...state,
                containers: action.data.containers
            };
        case constants.container.CONTAINER_QUEUE_UPDATE_ERROR:
            return {
                ...state,
                errors: [...state.errors, action.data]
            };
        case constants.container.UPDATE_NAME_AND_ID_FILTER:
            return {
                ...state,
                nameAndIdFilter: action.data
            };
        case constants.container.TURN_STATE_OFF:
            return {
                ...state,
                filteredOutStates: [...state.filteredOutStates, action.data]
            };
        case constants.container.TURN_STATE_ON:
            return {
                ...state,
                filteredOutStates: state.filteredOutStates.filter(state => state !== action.data)
            };
        case constants.container.PIN_CONTAINER:
            return {
                ...state,
                pinnedContainerIds: [...state.pinnedContainerIds, action.data]
            };
        case constants.container.UNPIN_CONTAINER:
            return {
                ...state,
                pinnedContainerIds: state.pinnedContainerIds.filter(id => action.data !== id)
            };
        default:
            return state;
    }
};

export default container;
