import container from './container';
import { createSelector } from 'reselect';

const selectAllContainers = state => container.selectAll(state.container);
const selectContainerById = (containerId, state) => container.selectById(containerId, state.container);
const selectContainerStatisticsById = (containerId, state) => container.selectStatisticsById(containerId, state.container);
const selectContainerNameAndIdFilter = state => container.selectNameAndIdFilter(state.container);
const selectAllContainerStates = state => container.selectAllStates(state.container);
const selectAllFilteredOutContainerStates = state => container.selectAllFilteredOutStates(state.container);
const selectPinnedContainerIds = state => container.selectPinnedIds(state.container);

const selectContainerCount = createSelector(
    selectAllContainers,
    containers => containers.length
);

const selectAllFilteredContainers = createSelector(
    selectAllContainers,
    selectContainerNameAndIdFilter,
    selectAllFilteredOutContainerStates,
    selectPinnedContainerIds,
    (containers, nameAndIdFilter, filteredOutStates, pinnedIds) => {
        nameAndIdFilter = nameAndIdFilter.trim().toLowerCase();
        return containers.filter(container => {
            return pinnedIds.includes(container.Id)
                || (!filteredOutStates.includes(container.State)
                    && (container.Id.toLowerCase().includes(nameAndIdFilter)
                        || container.Image.toLowerCase().includes(nameAndIdFilter)
                        || container.Names.filter(name => name.toLowerCase().includes(nameAndIdFilter)).length > 0));
        });
    }
);

const selectContainerStateCounts = createSelector(
    selectAllContainers,
    selectAllContainerStates,
    (containers, states) => {
        const stateCounts = {};
        states.forEach(state => stateCounts[state] = containers.filter(container => state === container.State).length);
        return stateCounts;
    }
);

export default {
    selectAllContainers,
    selectContainerById,
    selectContainerCount,
    selectContainerStatisticsById,
    selectContainerNameAndIdFilter,
    selectAllFilteredContainers,
    selectAllContainerStates,
    selectAllFilteredOutContainerStates,
    selectContainerStateCounts,
    selectPinnedContainerIds
};