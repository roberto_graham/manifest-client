const selectAll = store => store.containers;
const selectById = (id, store) => selectAll(store).find(container => id === container.Id);
const selectStatisticsById = (id, store) => store.statistics[id];
const selectNameAndIdFilter = (store) => store.nameAndIdFilter;
const selectAllStates = (store) => [...new Set(selectAll(store).map(container => container.State))];
const selectAllFilteredOutStates = (store) => store.filteredOutStates;
const selectPinnedIds = (store) => store.pinnedContainerIds;

export default {
    selectAll,
    selectById,
    selectStatisticsById,
    selectNameAndIdFilter,
    selectAllStates,
    selectAllFilteredOutStates,
    selectPinnedIds
};