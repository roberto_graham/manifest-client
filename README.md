# manifest-client

![MicroBadger Size](https://img.shields.io/microbadger/image-size/robertograham/manifest-client.svg?style=flat-square) ![Docker Automated build](https://img.shields.io/docker/automated/robertograham/manifest-client.svg?style=flat-square) ![Docker Build Status](https://img.shields.io/docker/build/robertograham/manifest-client.svg?style=flat-square)

## Usage

### Run with docker-compose

Download [docker-compose.yml](https://bitbucket.org/robertograham/manifest-app/src/master/docker-compose.yml)

```bash
docker-compose up
```

### Run with docker-app

```bash
docker-app render robertograham/manifest | docker-compose -f - up
```

## Contributing

Pull requests cannot be accepted as its a university project. You may raise issues but please don't include code snippets

## Built with

* [React](https://github.com/facebook/react)

* [Redux](https://github.com/reduxjs/redux)

* [React Router](https://github.com/ReactTraining/react-router)

* [STOMP.js](https://github.com/stomp-js/stompjs)

* [RMWC - React Material Web Components](https://github.com/jamesmfriedman/rmwc)